package com.sigmotoa;

public class Circle extends Geometric {
    public Circle(String figurename, int sidesnumber, double side) {
        super(figurename, sidesnumber, side);
    }

    @Override
    public void areaCalculation()
    {
        area = Math.PI * Math.pow(this.side, 2);
    }

    @Override
    public void perimeterCalculation() {
        perimeter = 2* Math.PI * this.side;
    }
    @Override
    public void volumeCalculation() {
        volume  = 4/3 * Math.PI * Math.pow(this.side,3);
    }


}
