package com.sigmotoa;

public class Triangle extends Geometric {
    public Triangle(String figurename, int sidesnumber, double side) {
        super(figurename, sidesnumber, side);
    }

    private double base;
    private double height;
    @Override
    public void areaCalculation()
    {
        area = this.base * this.height/2;
    }

    @Override
    public void perimeterCalculation()
    {
        perimeter= this.side + this.side + this.side;
    }

    @Override
    public void volumeCalculation() {
        volume = 1/3 * this.height * this.base;
    }
}

